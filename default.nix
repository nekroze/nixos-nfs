{config, lib, pkgs, ...}:
with lib;

let
  cfg = config.modules.nfs;
  mkFilesystem = name: target: {
    device = target.path;
    options = ["bind"];
  };
  mkNFSexport = name: target: "${name} ${target.clients}(${target.opts})\n";
  shareOptions = { name, ... }: {
    options = {
      clients = mkOption {
        type = types.string;
        default = cfg.clients;
        description = "NFS export client machine definition.";
      };
      opts = mkOption {
        type = types.string;
        default = "rw,nohide,insecure,no_subtree_check";
        description = "NFS export options.";
      };
      path = mkOption {
        type = types.string;
        description = "Path to map to this share.";
      };
    };
  };
in {
  options = {
    modules.nfs = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Enable the nix managed NFS server.";
      };
      clients = mkOption {
        type = types.string;
        default = "*";
        description = "NFS export client machine definition to be used by default.";
      };
      shares = mkOption {
        default = {};
        type = types.attrsOf (types.submodule shareOptions);
        example = literalExample ''
        {
          # Export via nfs from /mnt/home to /export/home.
          "/export/home" = { path = "/mnt/home"; };
        };
        '';
        description = "Attribute set of share maps to expose.";
      };
    };
  };

  config = mkIf cfg.enable {
    fileSystems = mapAttrs mkFilesystem cfg.shares;
    networking.firewall.allowedTCPPorts = [111 2049 4000 4001 4002];
    networking.firewall.allowedUDPPorts = [111 2049 4000 4001 4002];
    services.nfs = {
      server = {
        enable = true;
        exports = concatStringsSep "\n" (attrValues (mapAttrs mkNFSexport cfg.shares));
        statdPort = 4000;
        lockdPort = 4001;
        mountdPort = 4002;
      };
    };
  };
}
