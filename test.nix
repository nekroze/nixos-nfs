import <nixpkgs/nixos/tests/make-test.nix> {
  machine = { config, pkgs, ... }: {
    imports = [
      ./default.nix
    ];
    modules.nfs = {
      enable = true;
    };
  };

  testScript = ''
    subtest "nfs server is listening", sub {
      $machine->waitForOpenPort(111);
      $machine->waitForOpenPort(2049);
    }
  '';
}
